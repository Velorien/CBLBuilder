﻿using GalaSoft.MvvmLight;

namespace CBLBuilder.ViewModels
{
    public class ContentBlock : ViewModelBase
    {
        private bool selected;

        public string Name { get; set; }

        public bool Selected
        {
            get { return selected; }
            set { Set(() => Selected, ref selected, value); }
        }
    }
}
