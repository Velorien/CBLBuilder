﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;
using System.Linq;

namespace CBLBuilder.ViewModels
{
    public class ListMergerViewModel : ViewModelBase
    {
        private const string FieldsetsKey = "fieldsets";
        private const string LabelKey = "label";

        private string firstListJson;
        private string secondListJson;
        private string mergedJson;
        private ObservableCollection<ContentBlock> firstList = new ObservableCollection<ContentBlock>();
        private ObservableCollection<ContentBlock> secondList = new ObservableCollection<ContentBlock>();

        public ListMergerViewModel()
        {
            SelectDeselect = new RelayCommand<ObservableCollection<ContentBlock>>(SelectDeselectExecute);
            LoadJson = new RelayCommand(LoadJsonExecute);
            GenerateOutput = new RelayCommand(GenerateOutputExecute);
        }

        public RelayCommand<ObservableCollection<ContentBlock>> SelectDeselect { get; private set; }

        public RelayCommand LoadJson { get; private set; }

        public RelayCommand GenerateOutput { get; private set; }

        public string FirstListJson
        {
            get { return firstListJson; }
            set { Set(() => FirstListJson, ref firstListJson, value); }
        }

        public string SecondListJson
        {
            get { return secondListJson; }
            set { Set(() => SecondListJson, ref secondListJson, value); }
        }

        public string MergedJson
        {
            get { return mergedJson; }
            set { Set(() => MergedJson, ref mergedJson, value); }
        }

        public ObservableCollection<ContentBlock> FirstList
        {
            get { return firstList; }
            set { Set(() => FirstList, ref firstList, value); }
        }

        public ObservableCollection<ContentBlock> SecondList
        {
            get { return secondList; }
            set { Set(() => SecondList, ref secondList, value); }
        }

        private void SelectDeselectExecute(ObservableCollection<ContentBlock> list)
        {
            bool anySelected = list.Any(x => x.Selected);
            foreach(var item in list)
            {
                item.Selected = !anySelected;
            }
        }

        private void LoadJsonExecute()
        {
            FirstList.Clear();
            SecondList.Clear();

            try
            {
                var json = JObject.Parse(FirstListJson);
                FirstList = new ObservableCollection<ContentBlock>(json["fieldsets"]
                    .Select(x => new ContentBlock() { Name = x["label"].Value<string>() }).OrderBy(x => x.Name));
            }
            catch
            {
                MergedJson = "Invalid input data in the first list.";
                return;
            }

            try
            {
                var json = JObject.Parse(SecondListJson);
                SecondList = new ObservableCollection<ContentBlock>(json["fieldsets"]
                    .Select(x => new ContentBlock() { Name = x["label"].Value<string>() }).OrderBy(x => x.Name));
            }
            catch
            {
                MergedJson = "Invalid input data in the second list.";
            }
        }

        private void GenerateOutputExecute()
        {
            try
            {
                var firstJson = JObject.Parse(FirstListJson);
                var secondJson = JObject.Parse(SecondListJson);

                var tokensToRemoveFirstList = FirstList.Where(x => !x.Selected).Select(x => x.Name);
                var tokensToRemoveSecondList = SecondList.Where(x => !x.Selected).Select(x => x.Name);
                foreach (string token in tokensToRemoveFirstList)
                {
                    firstJson[FieldsetsKey].First(x => x[LabelKey].Value<string>() == token).Remove();
                }

                foreach (string token in tokensToRemoveSecondList)
                {
                    secondJson[FieldsetsKey].First(x => x[LabelKey].Value<string>() == token).Remove();
                }

                foreach (var item in secondJson[FieldsetsKey])
                {
                    firstJson[FieldsetsKey].Children().Last().AddAfterSelf(item);
                }

                MergedJson = firstJson.ToString();
            }
            catch
            {
                MergedJson = "Invalid input data.";
            }
        }
    }
}
