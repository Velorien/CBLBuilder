﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;
using System.Linq;

namespace CBLBuilder.ViewModels
{
    public class SublistCreatorViewModel : ViewModelBase
    {
        private string inputJson = "paste your master list here";
        private string outputJson;
        private ObservableCollection<ContentBlock> contentBlocks = new ObservableCollection<ContentBlock>();

        public SublistCreatorViewModel()
        {
            LoadJson = new RelayCommand(PopulateContentBlocks);
            GenerateOutput = new RelayCommand(GenerateOutputExecute);
            DeselectAll = new RelayCommand(DeselectAllExecute);
        }

        public RelayCommand LoadJson { get; private set; }

        public RelayCommand GenerateOutput { get; private set; }

        public RelayCommand DeselectAll { get; private set; }

        public string InputJson
        {
            get { return inputJson; }
            set { Set(() => InputJson, ref inputJson, value); }
        }

        public string OutputJson
        {
            get { return outputJson; }
            set { Set(() => OutputJson, ref outputJson, value); }
        }

        public ObservableCollection<ContentBlock> ContentBlocks
        {
            get { return contentBlocks; }
            set { Set(() => ContentBlocks, ref contentBlocks, value); }
        }

        private void PopulateContentBlocks()
        {
            try
            {
                var json = JObject.Parse(InputJson);
                ContentBlocks?.Clear();
                ContentBlocks = new ObservableCollection<ContentBlock>(json["fieldsets"]
                    .Select(x => new ContentBlock() { Name = x["label"].Value<string>() }).OrderBy(x => x.Name));
            }
            catch
            {
                OutputJson = "Invalid input data.";
            }
        }

        private void GenerateOutputExecute()
        {
            try
            {
                var json = JObject.Parse(InputJson);
                var tokensToRemove = ContentBlocks.Where(x => !x.Selected).Select(x => x.Name);
                foreach (string token in tokensToRemove)
                {
                    json["fieldsets"].First(x => x["label"].Value<string>() == token).Remove();
                }

                OutputJson = json.ToString();
            }
            catch
            {
                OutputJson = "Invalid input data.";
            }
        }

        private void DeselectAllExecute()
        {
            foreach(var item in ContentBlocks)
            {
                item.Selected = false;
            }
        }
    }
}
